Yocto Tasks
===========

.. NOTE::
  This file is written in `<reStructuredText>` format.
  See http://docutils.sourceforge.net/docs/user/rst/cheatsheet.html
  for quick help.

.. NOTE::
  Hopefully, this list is organized in the correct priority order, from high to low.

High Priority
~~~~~~~~~~~~~

- Yocto: Rebuild from scratch with ``MACHINE=x86`` CPU (``meta-intel`` layer)

- BCM-SDK: Reduce huge ``-g`` binary sizes (``-gsplit-dwarf``, ``-Wl,--gdb-index``, ``-fuse-ld=gold``)

  - Huge sections: ``.debug_info``, ``.rel.debug_info``, ``.debug_str``

  - Note: https://www.roguewave.com/sites/rw/files/attachments/RW-Saving-Time-and-Space-Split-DWARF-WP-20170525.pdf

  - Compiling fix:
    ::

      Use the “-gsplit-dwarf” compiler option to generate “.dwo” (DWARF
      object) files containing the full DWARF debug information, and a ".o"
      (object) file containing the code, data, and skeleton DWARF debug
      information.  Note: As shown below, the DWARF debug information in the
      “.o” file points to the “.dwo” file, therefore the “.dwo” file must
      not be deleted to debug that module.

  - Linking fix:
    ::

      Use the “-fuse-ld=gold” compiler option to use the gold linker
      (ld.gold).  Use the “-Wl,--gdb-index” compiler option to pass the
      “--gdb-index” option to the gold linker.  Note: The resulting
      executable or shared library image file will contain a “.gdb_index”
      section that the debugger can use for faster startup.

- BCM-SDK: Fix: ``bitbake core-image-full-cmdline -c populate_sdk_ext``
  ::

    Exception: FileNotFoundError: [Errno 2] No such file or directory:
    '/home/kosta/dev/yocto-agema-repo/build/tmp/work/qemux86-poky-linux/\
    core-image-full-cmdline/1.0-r0/sdk-ext/image//opt/poky/2.5.1/conf/local.conf.bak' ->
    '/home/kosta/dev/yocto-agema-repo/build/tmp/work/qemux86-poky-linux/\
    core-image-full-cmdline/1.0-r0/sdk-ext/image//opt/poky/2.5.1/conf/local.conf'

- Kernel: Rebase to Yocto Linux Kernel 4.14.y

  - Make Yocto to use custom Linux Kernel 4.14.y

  - Rebuild BCM-SDK with Linux Kernel 4.14.y

  - Rebuild all from scratch with Linux Kernel 4.14.y

- Yocto: Port Ubuntu 12.04 RootFS customizations to Yocto RootFS

- Fix: BCM-SDK: Install kernel objects in a proper directory

  - Current: ``/deploy/linux-kernel-bde.ko``

  - Correct: ``/lib/modules/4.14.48-yocto-standard/kernel/...``

  - Remove ``linux-kernel-bde.ko`` and ``linux-user-bde.ko`` from ``main``

- Main: BCM-SDK: Remove all BCM-SDK artifacts from ``main``

  - Especially including ``git history`` (use ``git filter-branch``)

Medium Priority
~~~~~~~~~~~~~~~

- Yocto: Docker: Add docker on-target support

- BCM-SDK: Make it build fast (now takes 45 mins)

- Main: Fix slow ``main`` build process (now takes 25 min instead of 15 min)

  - Try ``-j`` for sub-parallel build

- Main: Fix ``i2c_agema`` file ``Make.mk`` to use modular ``-L`` paths

- Main: Replace ``INSTALL_PATH?=../../open_clovis/OP9500/src/extras/bin`` with proper define in:

  - platform/drivers/i2c_agema/Make.mk

  - platform/drivers/rw_ha_fpga/Makefile

  - platform/drivers/upgrade_client/Makefile

  - platform/drivers/io_ha_fpga/Makefile

- Main: Never touch host machine

  - Main: Remove access to ``/usr/include/i386-linux-gnu`` of the host machine

  - Main: Remove access to ``/usr/lib`` of the host machine

  - Main: Remove access to ``/usr/local/include`` of the host machine

  - Main: Remove access to ``/home/mrv`` of the host machine

  - Main: Remove all instances of ``/usr/lib/perl/5.14/`` in ``main``

  - Main: Fix ``EXTRA_SHARED_LDFLAGS`` (``EXTRA_SHARED_LDFLAGS=-L$(ASP_LIB) -L/usr/local/lib -lpthread``)

Low Priority
~~~~~~~~~~~~

- Main: Replace all ``i686-nptl-linux-gnu`` instances in ``main`` with modular approach

- Main: SAFplus: Replace all ``clovis_2015_lk4_4`` strings to ``clovis`` in ``main`` project

- Yocto: SAFplus: Fix recipe install warning
  ::

    is owned by uid 1000, which is the same as the user running bitbake.
    This may be due to host contamination

- Yocto: BCM-SDK: Fix recipe install warning
  ::

    is owned by uid 1000, which is the same as the user running bitbake.
    This may be due to host contamination

- Main: Remove trash ``platform/open_clovis/OP9500/src/extras/bin/net-snmp-config``

- Main: Remove ``app/aaa/pam-1.1.3_ref_only`` from ``main``

- Main: Remove all instances of ``plastic`` in ``main``

- Main: Remove all binary files from ``main``

- Main: Clean git history (``git filter-branch``)

  - Remove BCM-SDK binaries

  - Remove FPGA binaries

  - Remove **all** remaining binaries

- Yocto: How to create the below files?

  - libevent_openssl.so

  - libevent_pthreads.so

- Main: Fix version files (``build_opx.py``) and ``version`` directory

Future Tasks
~~~~~~~~~~~~

- Future: SAFplus: Replace with ``opensaf`` package (check ``meta-cgl`` layer)

  - OpenSAF: Make fresh **separate** yocto build with ``opensaf`` package

- Main: Split ``main`` git to SAFplus agents and all the other stuff

- BCM-SDK: Fix all compilation warnings and add ``-Werror`` GCC flag (Make all warnings into errors)

- Main: Fix all compilation warnings and add ``-Werror`` GCC flag (Make all warnings into errors)
